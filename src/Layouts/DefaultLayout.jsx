import React from "react";
import {Link} from "react-router-dom";


// <> </ > 파편 => 교체될수 있음을 나타냄
const DefaultLayout = ({children}) => {
    return(
        <>
            <div className={"header"}> 헤더
            <nav>
                <Link to={"/"}>Page1</Link>
                <Link to={"/p2"}>Page2</Link>
                <Link to={"/p3"}>Page3</Link>
                <Link to={"/p4"}>Page4</Link>
            </nav>
            </div>
            <main>{children} </main>
            <footer> footer </footer>
        </>
    )
}

export default DefaultLayout