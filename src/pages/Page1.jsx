import React, {useState} from "react";


const Page1 = () => {
    const [name1,setName1] = useState("")
    const [name2,setName2] = useState("")
    const [result, setResult] = useState([])
    const [resultPercent , setResultPercent] = useState(0)

    const onChangeHandler = (e) => {
        const {value , name } = e.target
        switch (name) {
            case "name1":
                setName1(value)
                console.log("name1:" + value)
                return
            case "name2":
                setName2(value)
                console.log("name2:" +value)
                return;
        }

        console.log("name1:"+e.target.value.name)
        console.log("name2:"+e.target.value.name)
    }

    const onSubmit = (e) => {
        e.preventDefault();
        setResult([...result, {id: result.length + 1 , name1 ,name2}])
        console.log(result)
    }

    const getRandom = () => {
        const calculate = Math.random() * (100) ;
        setResultPercent(Math.floor(calculate))}


    return (
        <div>
           <form onSubmit={onSubmit}>
            <input name={"name1"} type={"text"} value={name1} onChange={onChangeHandler} />
            <input name={"name2"} type={"text2"} value={name2} onChange={onChangeHandler} />
               <button onClick={getRandom} type={"submit"} > 궁합도 보기 </button>
           </form>
            <div>
                {name1} 님과 {name2} 의  궁합은  {resultPercent} % 입니다.
            </div>


        </div>
    )
}

export default Page1