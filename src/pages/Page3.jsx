import React, {useState} from "react";

const Page3 = () => {
    const INITIAL_CASH = 50000
    const BOX_PRICE = 5000
    const [cash , setCash] = useState(INITIAL_CASH)
    const [result , setResult] = useState('')
    const [resultList , setResultList] = useState([])
    const [gameOver , setgameOver] =useState(true)

    const onClick = () => {
        setCash(cash - BOX_PRICE)
        if (cash <= BOX_PRICE ) setgameOver(false)
        const resultScore = Math.floor(Math.random() * 100)
        if (0 < resultScore && resultScore <= 1){
            setResult('홀란드')
            getRecord(result)
        } else if (1 <resultScore && resultScore < 30){
            setResult('손흥민')
            getRecord(result)
        } else {
            setResult('이강인')
            getRecord(result)
        }

        console.log(resultList)
    }
    const getRecord = (result) => {
        resultList.push(result)
    }

    const onReset = () => {
        setCash(INITIAL_CASH)
        setResult('')
        setResultList([])
        setgameOver(true)
    }

    return(
        <div>
            상자깡 <br/>
            보유현금 : {cash} 원 <br/>
            <button onClick={onClick} disabled={!gameOver} > 상자 까기</button>
            <button onClick={onReset}> 다시 하기 </button>
            <div>
                상자깡 결과 <br/>
                {result}
                {resultList.map((value, index) =>
                    index !==0  ?
                <div key={index + 1}>
                    <p>{index} . {value}</p>
                </div> : ''
                )}
            </div>
        </div>
    )

}

export default Page3