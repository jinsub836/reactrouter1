import React, {useState} from "react";

const Page4  = () =>{
    const [Money , setMoney] = useState(0)
    const [name1 , setName1] = useState('')
    const [name2 , setName2] = useState('')
    const [name3 , setName3] = useState('')
    const [result , setResult] = useState([])

    const onChangeHandleMoney = (e) => {
        setMoney(e.target.value)
    }

    const onChangeHandleName = (e) => {
        const {value , name} = e.target
        switch (name){
            case ('name1') :
                setName1(value)
                return;
            case ('name2') :
                setName2(value)
                return;
            case ('name3') :
                setName3(value)
                return;
        }
    }

    const caculateMoney = (e) => {
        let percent = Math.floor(Math.random() * (101))
        let percent2 = Math.floor(Math.random() * (100 -percent))
        let percent3 = 100-percent2-percent

        const price1 = Money*percent / 100
        const price2 = Money*percent2 / 100
        const price3 = Money*percent3 / 100

        console.log(price1+price2+price3 , Money)

        if( percent+percent2+percent3 === 100 ){
        setResult([price1,price2,price3]);
        console.log(result)
        }else {
            alert("다시해봅시다")
        }
    }
    const reset = () =>{
        setMoney(0)
        setResult([])
    }

    return(
        <div>
            금액을 입력하세요. <input type={"text"} value={Money} onChange={onChangeHandleMoney}/> <br/>
            총 금액 : {Money} <br/>
            <div><br/>
                <span> <input type={"text"} name={'name1'} value={name1} onChange={onChangeHandleName}/></span>
                <span> <input type={"text"} name={'name2'} value={name2} onChange={onChangeHandleName}/></span>
                <span> <input type={"text"} name={'name3'} value={name3} onChange={onChangeHandleName}/></span>
            </div>
            <button onClick={caculateMoney}> N 빵하기</button>
            <button onClick={reset}> 다시 하기</button>
            <div>
                <p>{name1} : {result[0]} 원 </p>
                <p>{name2} : {result[1]} 원 </p>
                <p>{name3} : {result[2]} 원 </p>
            </div>
        </div>
    )

}
export default Page4