import React, {useState} from "react";

const Page2 = () => {
    const [clothTop, setClothTop] = useState('')
    const [clothBottom, setClothBottom] = useState('')
    const [clothTopPick , setClothTopPick] = useState("")
    const [clothBotPick , setClothBotPick] = useState("")

    const onChangeHandlerTop = (e) => {
        setClothTop(e.target.value)
    }


    const onChangeHandlerBot = (e) => {
        setClothBottom(e.target.value)
    }

    const pickCloth = () => {
        const topArray = clothTop.split(',')

        const botArray = clothBottom.split(',')

        setClothTopPick(topArray[Math.floor(Math.random() * (topArray.length))])
        setClothBotPick(botArray[Math.floor(Math.random() * (botArray.length))])
    }


    return(
        <div>
                <label htmlFor={"clothTop"}> 상의 </label>
                <input type={"text"} id={"clothTop"} name={"clothTop"} value={clothTop} onChange={onChangeHandlerTop}/>
                <label htmlFor={"clothBottom"}> 하의 </label>
                <input type={"text"} id={"clothBottom"} name={"clothBottom"} value={clothBottom} onChange={onChangeHandlerBot}/>
                <button  onClick={pickCloth} > 내일 입을 옷 정하기 ! </button>
            <div>
                내일은 {clothTopPick}, {clothBotPick} 를 입어요 !
            </div>
        </div>
    )
}

export default Page2